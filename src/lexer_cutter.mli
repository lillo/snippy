val buffer_size : int
val snippet_description : Buffer.t
val snippet_buffer : Buffer.t
val description_flag : bool ref
val snippet_list : Snippet.snippet list ref
val preamble_list : string list ref
val return_data : unit -> string * string
val reset_snippet_list : unit -> unit
val reset_preamble_list : unit -> unit
val insert : string -> unit
val __ocaml_lex_tables : Lexing.lex_tables
val next_snippet :
  < get_snippet_by_path : string -> string -> ('a -> bool) -> Snippet.snippet;
    .. > ->
  Lexing.lexbuf -> string * string
val __ocaml_lex_next_snippet_rec :
  < get_snippet_by_path : string -> string -> ('a -> bool) -> Snippet.snippet;
    .. > ->
  Lexing.lexbuf -> int -> string * string
val cut_snippet :
  < get_snippet_by_path : string -> string -> ('a -> bool) -> Snippet.snippet;
    .. > ->
  Lexing.lexbuf -> string * string
val __ocaml_lex_cut_snippet_rec :
  < get_snippet_by_path : string -> string -> ('a -> bool) -> Snippet.snippet;
    .. > ->
  Lexing.lexbuf -> int -> string * string
