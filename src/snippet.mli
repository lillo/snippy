val optional_arg : 'a -> 'a option -> 'a
val check_param : 'a -> 'a -> string -> unit
val check_empty : string -> string -> unit
val bool_optional_arg : bool option -> bool
val int64_optional_arg : Int64.t option -> Int64.t
class author :
  ?id:Int64.t ->
  string ->
  string ->
  string ->
  object
    method get_email : unit -> string
    method get_id : unit -> Int64.t
    method get_name_last_name : unit -> string
    method get_nickname : unit -> string
  end
class ccode :
  ?id:Int64.t ->
  string ->
  object
    val mutable identifier : Int64.t
    method get_id : unit -> Int64.t
    method get_text : unit -> string
    method set_id : Int64.t -> unit
  end
class cexception :
  ?id:Int64.t ->
  string ->
  string ->
  string ->
  object
    val mutable identifier : Int64.t
    method get_constructor_name : unit -> string
    method get_constructor_type : unit -> string
    method get_id : unit -> Int64.t
    method get_text : unit -> string
    method set_id : Int64.t -> unit
  end
class ctype :
  ?id:Int64.t ->
  string ->
  ?polimorphic:bool ->
  ?variant:bool ->
  ?module_signature:bool ->
  ?class_type:bool ->
  string ->
  object
    val mutable identifier : Int64.t
    method get_id : unit -> Int64.t
    method get_name : unit -> string
    method get_text : unit -> string
    method is_class_type : unit -> bool
    method is_module_signature : unit -> bool
    method is_polimorphic : unit -> bool
    method is_variant : unit -> bool
    method set_id : Int64.t -> unit
  end
class cclass :
  ?id:Int64.t ->
  string ->
  ?is_virtual:bool ->
  string ->
  object
    val mutable identifier : Int64.t
    method get_id : unit -> Int64.t
    method get_name : unit -> string
    method get_text : unit -> string
    method is_virtual : unit -> bool
    method set_id : Int64.t -> unit
  end
class cmodule :
  ?id:Int64.t ->
  string ->
  string ->
  string ->
  object
    val mutable identifier : Int64.t
    method get_alias : unit -> string
    method get_id : unit -> Int64.t
    method get_name : unit -> string
    method get_text : unit -> string
    method set_id : Int64.t -> unit
  end
class cfunction :
  ?id:Int64.t ->
  string ->
  string ->
  string ->
  object
    val mutable identifier : Int64.t
    val mutable tests : (string * string) list
    method get_id : unit -> Int64.t
    method get_name : unit -> string
    method get_tests : unit -> (string * string) list
    method get_text : unit -> string
    method get_type : unit -> string
    method set_id : Int64.t -> unit
    method set_tests : (string * string) list -> unit
  end
type code =
    Code of ccode
  | Function of cfunction
  | Exception of cexception
  | Type of ctype
  | Class of cclass
  | Module of cmodule
val code2ccode : code -> ccode
class snippet :
  ?id:Int64.t ->
  ?parent:Int64.t ->
  ?deprecated:bool ->
  ?snippet_tree:Int64.t ->
  string ->
  string ->
  string ->
  string list ->
  string list ->
  string ->
  string ->
  string ->
  author list ->
  int64 list ->
  code list ->
  object
    method get_authors : unit -> author list
    method get_brief : unit -> string
    method get_code : unit -> code list
    method get_depend_list : unit -> int64 list
    method get_dir : unit -> string
    method get_id : unit -> Int64.t
    method get_keywords : unit -> string list
    method get_licenses : unit -> string list
    method get_name : unit -> string
    method get_parent : unit -> Int64.t
    method get_path : unit -> string
    method get_signature : unit -> string
    method get_snippet_tree : unit -> Int64.t
    method get_use_example : unit -> string
    method get_version : unit -> string
    method is_deprecated : unit -> bool
    method to_text : unit -> string
  end
val equals :
  < get_id : unit -> Int64.t; .. > ->
  < get_id : unit -> Int64.t; .. > -> bool
