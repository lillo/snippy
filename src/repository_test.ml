open Snippet

module Db = Dbi_mysql

(*
  L'autore di tutti gli snippet di prova
*)

let me = new author "lillo" "Letterio Galletta" "galletta@cli.di.unipi.it"

(*
  Costruisce uno snippet che contiene il codice che implementa
  una lista con delle operazioni molto elementari; il codice 
  ocaml della lista e' il seguente:

type 'a lista = Full of ('a * 'a lista) | Empty

exception EmptyListException

let insert el lista = Full(el,lista)

let head = function 
  Full(h,t) -> h
  | Empty -> raise EmptyListException

let tail = function
    Full(h,t) -> t
  | Empty -> raise EmptyListException

let rec iter f = function
    Full(h,t) -> f h; iter f t
  | Empty -> ()

*)

let createListSnippet () = 
  let listType = new ctype "lista" ~polimorphic:true 
    "type 'a lista = Full of ('a * 'a lista) | Empty"
  and listEx = new cexception "EmptyListException" "" 
    "exception EmptyListException"
  in
  let listInsert = new cfunction "insert" "'a -> 'a lista -> 'a lista"
    "let insert el lista = Full(el,lista)"
  and listHead = new cfunction "head" "'a lista -> 'a"
    "let head = function\nFull(h,t) -> h\n| Empty -> raise EmptyListException"
  and listTail = new cfunction "tail" "'a lista -> 'a lista" 
    "let tail = function\nFull(h,t) -> t\n| Empty -> raise EmptyListException"
  and listIter = new cfunction "iter" "'a -> unit -> 'a lista -> unit"
    "let rec iter f = function\nFull(h,t) -> f h; iter f t\n| Empty -> ()"
  in
    listInsert#set_tests [("10 Full(9,Empty)","Full(10,Full(9,Empty))")];
    new snippet "SimpleList" 
    "Implementazione di una semplice lista non modificabile"
    "0.1" ["lista"] ["GPL"] "let l = insert 10 Empty in iter (print_int) l" 
    "dataStruct" "" [me] []
    [Type(listType);Exception(listEx);Function(listInsert);
     Function(listHead); Function(listTail); Function(listIter)]

(*
  Costruisce uno snippet che implementa una classe point con coordinate
  intere e che ha una funzione di utilita' che serve a stampare un punto
  sullo schermo il cui codice e':

  class point x y =
    object(self)
      method getX() = x
      method getY() = y
      method traslate dx dy = new point (dx+x) (dy+y)
    end

  let draw_point p = 
    print_string "(";
    print_int (p#getX());
    print_string ",";
    print_int (p#getY());
    print_string ")"
*)

let createPointSnippet () = 
  let pointClass = new cclass "point" 
    ("class point x y =\nobject(self)\nmethod getX() = x\nmethod getY() = y\n" ^
    "method traslate dx dy = new point (dx+x) (dy+y)\nend")
  in
  let drawFunction = new cfunction "draw_point"
    "point -> unit"
    ("let draw_point p = \nprint_string \"(\";\nprint_int (p#getX());print_string \",\";\n"
    ^ "print_int (p#getY());\nprint_string \")\"")
  in
    new snippet "punto" "Una semplice implementazione di un punto a coordinate intere"
      "0.1" ["punto"] ["GPL"] "let p = new point 10 15 in p#traslate 5 5" 
      "mathematic" "" [me] [] [Class(pointClass); Function(drawFunction)]

(*
  Costruisce uno snippet che implementa una classe polygon molto elementare 
  e stupida, il cui codice e' il seguente:

  class virtual polygon vertices = 
    object(self)
      val v_list = (vertices : point lista)
      method virtual area: unit -> float
      method draw_poly () =
        iter draw_point v_list
    end
*)

let createPolygonSnippet (repo: Repository.db_repository) id1 id2= 
  let polyClass = new cclass "polygon" ~is_virtual:true
    ("class virtual polygon vertices =\nobject(self)\nval v_list = (vertices: point lista)\n"
    ^ "method virtual area: unit -> float\n" ^
    "method draw_poly () = \niter draw_point v_list\nend")
  in
  new snippet "poligono" "Una semplice implementazione di un poligono"
    "0.1" ["Poligono"] ["GPL"] "" "mathematic" "" [me] [id1;id2] [Class(polyClass)]
    
				 
      
let main () = 
  let dbh = new Db.connection "SnippetsManager" ~user:"snippets" ~password:""
  in
  let repohandle = new Repository.db_repository dbh
  in
  let listaSnippet = createListSnippet ()
  in
    print_string "\n\n****\n INSERISCO LISTA \n***\n\n";
    let id1 = repohandle#insert_snippet listaSnippet in 
    let pointClass = createPointSnippet ()
    in
      print_string "\n\n****\n INSERISCO POINT \n***\n\n";
      let id2 = repohandle#insert_snippet pointClass in
      print_string "\n\n****\n CREO POLYGON \n***\n\n";
      let polyClass = createPolygonSnippet repohandle id1 id2
      in
	print_string "\n\n****\n INSERISCO POLYGON \n***\n\n";
	let poly = repohandle#insert_snippet polyClass in 
	let poly = repohandle#get_snippet_by_id poly in
    let clos = repohandle#get_depend_closure poly (fun x -> true) in
      List.iter (function s -> Printf.printf "\n\n****** %s ******\n" (s#to_text())) (clos @ [ poly])

let _= main ()

  
    
