{ 
	open Snippet
	
  let buffer_size = 512
  let snippet_description = Buffer.create buffer_size
  let snippet_buffer = Buffer.create buffer_size
  let description_flag = ref(false)
	let snippet_list = ref([])
	let preamble_list = ref([])

  let return_data () = 
    let (descr, data) = (
      Buffer.contents snippet_description ,
      Buffer.contents snippet_buffer
    ) 
    in
      Buffer.reset snippet_description;
      Buffer.reset snippet_buffer;
			let snippet = Printf.sprintf "%s\n%s\n" descr data in
			preamble_list := !preamble_list @ [snippet];
      (descr, data)

	let reset_snippet_list () = 
		snippet_list := []

	let reset_preamble_list () = 
		preamble_list := []

  let insert str =
    let buffer = 
      if !description_flag = true then
	snippet_description
      else
	snippet_buffer
    in
      Buffer.add_string buffer str

}

let snippet_begin = "(*!"
let snippet_end = '(''*''*'+'*'')'

let letter = ['a'-'z''A'-'Z']
let digit = ['0'-'9']
let ide = (letter) (letter | digit | '_')*
let matcher = "Snippet." (ide ".")* ide 
    

rule next_snippet repo = parse
  |snippet_begin
      {
	description_flag := true;
	insert "(**" ; 
	cut_snippet repo lexbuf 
      }
  |eof 
      { return_data () }
  | _ 
      { next_snippet repo lexbuf }
and cut_snippet repo = parse
  | "*)" as s
      { 
	insert s;
	description_flag := false;
	cut_snippet repo lexbuf
      }
  | snippet_begin 
      {
        insert "(**";
        cut_snippet repo lexbuf
      }
  | snippet_end 
      { return_data () }
  | eof 
      { description_flag := false; return_data () }
	| matcher as s 
		{
			let (sn:snippet) = Pp_lexer.get_snippet repo s in
			snippet_list := !snippet_list @ [sn];
			insert s;
			cut_snippet repo lexbuf	
		}
  | _ as c 
      { 
	insert (String.make 1 c);
	cut_snippet repo lexbuf
      }

{
}