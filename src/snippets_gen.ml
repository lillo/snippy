(**
 * La gestione degli autori deve essere migliorata
 * Inserzione delle classi.
 *)

open Odoc_info
open Odoc_info.Value
open Odoc_info.Type
open Odoc_info.Module
open Odoc_info.Exception
open Odoc_info.Class
open Odoc_info.Name

open Snippet
open Query
open Repository

exception Missing_tag_exception of string

let trim str = 
	let regexp = Str.regexp " \|\n\|\t" in
		Str.global_replace regexp "" (String.lowercase str)

let throw_missing_tag_exception tag_name () =
  raise (Missing_tag_exception (Printf.sprintf "Missing %s tag in your snippet header" tag_name))

       
let get_snippet_brief (info: Odoc_info.info) = 
   match info.i_desc with
     Some(brief) -> Odoc_info.string_of_text brief
     | _  -> throw_missing_tag_exception "brief" ()

 let get_author repo (nickname,name,email) = 
   try
     repo#get_author_by_nickname nickname
   with EmptyResult ->
     new author nickname name email
       
let get_single_custom_tag tag_name custom_tag_list f_result f_rescue = 
  try
    let text_element = List.assoc tag_name custom_tag_list in
      f_result ( Odoc_info.string_of_text text_element )
  with Not_found -> f_rescue ()
  
let get_multi_custom_tag tag_name custom_tag_list f_result f_rescue = 
  let filter (tag,_) = tag = tag_name in
  let res_list = List.filter filter custom_tag_list in
    if res_list = [] then
      f_rescue ()
    else
      let text_element_list = snd ( List.split res_list ) in
        f_result ( List.map Odoc_info.string_of_text text_element_list )
    
let get_single_optional_custom_tag tag_name custom_tag_list = 
  get_single_custom_tag tag_name custom_tag_list (fun x -> Some(x)) (fun () -> None)
    
let get_single_no_optional_custom_tag tag_name custom_tag_list =
  get_single_custom_tag tag_name custom_tag_list (fun x -> x) 
    (throw_missing_tag_exception tag_name)
  
let get_snippet_name custom_tag_list =
  trim (get_single_no_optional_custom_tag "s_name" custom_tag_list )
  
let get_snippet_version custom_tag_list = 
  get_single_optional_custom_tag "s_version" custom_tag_list
  
let get_snippet_directory custom_tag_list = 
  get_single_no_optional_custom_tag "s_directory" custom_tag_list 

let get_snippet_example custom_tag_list = 
  get_single_optional_custom_tag "s_example" custom_tag_list 

let get_snippet_keywords custom_tag_list =
  get_multi_custom_tag "s_keyword" custom_tag_list (fun x -> Some(x)) (fun x -> None)
  
let get_snippet_licences custom_tag_list = 
  get_multi_custom_tag "s_license" custom_tag_list (fun x -> Some(x)) (fun x -> None)

let get_snippet_author custom_tag_list = 
  match get_multi_custom_tag "s_author" custom_tag_list (fun x -> Some(x)) (fun x -> None) with
    Some(authors) ->
  let filter str =
    let regexp_author = Str.regexp "^ *[a-zA-Z]+ */ *[a-zA-z ]+ */ *.+ *$" in
      let r = Str.string_match regexp_author str 0 in
        if not r then
          (print_string ("Attenzione autore non inserito perche' non valido " ^ str ^"\n"); false)
        else
          true
    in
  let res_list = List.filter filter authors in
        let mapper str = 
          let reg1 = Str.regexp " */ *" 
          and reg2 = Str.regexp " \|\t\|\n" in
            let trim str = Str.global_replace reg2 "" str in
            let get_el el = List.nth (Str.split reg1 str) el in
            (trim (get_el 0), trim (get_el 1), trim (get_el 2))
        in
        List.map mapper res_list
          | _ -> []
        
  
let is_snippet_deprecated custom_tag_list =
  match get_single_optional_custom_tag "s_deprecated" custom_tag_list with
      None -> false
    | Some(_) -> true
      
let get_snippet_id_from_path (repo:db_repository) path =       
	let path = trim path in
  let bar = String.rindex path '/' in
	let snippet_name = String.sub path (bar + 1) (String.length path - bar - 1) in
	let dir_name = String.sub path 0 bar in
			Printf.fprintf stderr "*************\n\n QUI:%s \n\n*********\n\n" path;
      repo#get_snippet_tree_root_id snippet_name dir_name
      
let get_snippet_depend (repo:db_repository) custom_tag_list = 
  match get_multi_custom_tag "s_depend" custom_tag_list (fun x -> Some(x)) (fun x -> None) with
      None -> []
    | Some(l) -> List.map (get_snippet_id_from_path repo) l
        
let string_datetime2dbi_date_time str =
    let sub start len str = int_of_string (String.sub str start len) in
      let year    = sub 0  4 str   in
      let month   = sub 5  2 str   in 
      let day     = sub 8  2 str   in
      let hour    = sub 11 2 str   in
      let minute  = sub 14 2 str   in
      let second  = sub 17 2 str   in
        ({Dbi.year = year; Dbi.month=month; Dbi.day = day},
         {Dbi.hour = hour; Dbi.min = minute; Dbi.sec = second;
          Dbi.microsec = 0; Dbi.timezone = None})
      
let get_snippet_extends custom_tag_list = 
  match get_single_optional_custom_tag "s_extends" custom_tag_list with
      None -> None
    | Some(a) -> let validator = Str.regexp 
      "^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]$" in
        if not (Str.string_match validator a 0) then
          raise (Failure (Printf.sprintf "s_extends tag invalid format: %s" a))
        else
          Some( string_datetime2dbi_date_time a )
           
          
let get_module_alias custom_tag_list = 
  match get_single_optional_custom_tag "alias" custom_tag_list with
    Some(alias) -> alias
    | _ -> ""
  
let get_value_info value_t = 
  match value_t.val_info with
    Some(info) -> info
    | _ -> raise Not_found
      
let get_test_function custom_tag_list = 
  match get_multi_custom_tag "test" custom_tag_list (fun x -> Some(x)) (fun x -> None) with
    Some(list) ->
      let regexp = Str.regexp "^ *.+ *-> *.+ *$" in
      let filter el = 
        let r = Str.string_match regexp el 0 in
          if not r then
            (print_string "Attenzione test non inserito perche' non valido\n"; false)
          else
            true
      in
      let res_filter = List.filter filter list in
      let map el = 
        let regexp1 = Str.regexp " *-> *" 
        and regexp2 = Str.regexp "[ \t\n]*" in
          let g_repl = Str.global_replace regexp2 "" in
            let s = Str.split regexp1 el in
            (g_repl ( List.nth s 0 ) , g_repl (List.nth s 1 ))
      in
      List.map map res_filter
    | _ -> []
      
let get_code = function
  Some(code) -> code
  | _ -> assert false
    
let get_location_element (loc : location) =
  match loc.loc_impl with
    Some(file,offset) -> (file, offset)
    | _ -> assert false
    
let code2ccode = function
  Code(code) -> code 
  | Function(cfunction) -> (cfunction :> ccode)
  | Exception(cexception) -> (cexception :> ccode)
  | Type(ctype) -> (ctype :> ccode)
  | Class(cclass) -> (cclass :> ccode)
  | Module(cmodule) -> (cmodule :> ccode)
    
let set_required_code (code_element : ccode) code_list =
  ()
    
let get_module_path module_t = 		
		fst(get_location_element module_t.m_loc)

let extract_module string = 
  let pattern = Str.regexp_string "module Snippet_data :" in
  let pos = Str.search_forward pattern string 0 in
    String.sub string pos (String.length string - pos)
		
let rename_sig_mod signature snippet_name = 
	let pattern = Str.regexp_string "Snippet_data" in
	Str.global_replace pattern snippet_name signature

let my_run command file = 
  let cmd = Printf.sprintf "%s > %s" command file in
    if (Sys.command cmd) = 0 then
      let str = ref("")
      and file = open_in file in 
	try
	  while true do 
	    str := !str ^ (input_line file) ^ "\n"
	  done;
	  ""  
	with End_of_file -> !str
    else
      assert false
			
let get_signature module_t = 			
  let path = get_module_path module_t in
		let command = Printf.sprintf "ocamlc -i %s" path in
		let result = my_run command (Printf.sprintf "%si" path) in
		extract_module result 
		
let module_text_only module_t = 
  let file_path = get_module_path module_t in
    let file_name = String.capitalize (Filename.chop_extension (Filename.basename file_path)) in
      module_t.m_name <> file_name
 
let is_variant type_t = 
  match type_t.ty_kind with
    Type_variant(_) -> true
    | _ -> false
      
let is_abstract type_t = 
  match type_t.ty_kind with
    Type_abstract -> true
    | _ -> false
    
type snippet_data = { 
  name: string; 
  brief: string;
  version: string option;
  keywords: string list option;
  licences: string list option;
  use_example: string option;
  is_deprecated: bool;
  path: string ;
  parent_timestamp: Dbi.datetime option;
  authors: (string * string * string) list;
	signature: string;
  depend_list: int64 list
 }
  
	
let create_snippet_by_data snippet_data code_list (repo : db_repository)= 
let version = match snippet_data.version with
    Some(v) -> v
    | _ -> "1.0"
and keywords = match snippet_data.keywords with
  Some(ks) -> ks
  | _ -> []
and licenses = match snippet_data.licences with
  Some(ls) -> ls
  | _ -> []
and use_example = match snippet_data.use_example with
  Some(ex) -> ex
  | _ -> ""
and parent = match snippet_data.parent_timestamp with
  Some(ts) ->  repo#get_specific_snippet_id snippet_data.name snippet_data.path ts
  | None -> Int64.of_int(-1) 
and snippet_tree = 
	try
  repo#get_snippet_tree_root_id snippet_data.name snippet_data.path    
      
  with EmptyResult -> Int64.of_int(-1)
	and signature = rename_sig_mod snippet_data.signature (String.capitalize snippet_data.name)
and authors = List.map (get_author repo) snippet_data.authors
in
    new snippet snippet_data.name snippet_data.brief ~deprecated:snippet_data.is_deprecated
          ~parent:parent ~snippet_tree:snippet_tree
          version keywords licenses use_example snippet_data.path signature authors snippet_data.depend_list code_list

let delete_module_name module_name str = 
  let reg_exp = Str.regexp ( Printf.sprintf "%s\." module_name) in
    Str.global_replace reg_exp "" str
 
let mystring_of_type_expr module_name type_expr = 
  delete_module_name module_name ( Odoc_info.string_of_type_expr type_expr )
      
let mystring_of_type_list module_name sep list = 
try
  let initial = mystring_of_type_expr module_name ( List.hd list ) in
  let f_aux p_res el = 
    Printf.sprintf "%s%s%s" p_res sep (mystring_of_type_expr module_name el)
  in
      List.fold_left f_aux initial ( List.tl list )
  with Failure(s) -> ""
      
let get_module_info module_t = 
  match module_t.m_info with
    Some(info) -> info
    | None -> raise Not_found
  
class snippet_gen ( repository : db_repository) =
object(self)
  inherit Odoc_info.Scan.scanner as super
    
  val mutable code_elements = []
  val mutable last_snippet_data = None
  val mutable module_name = ""
	val mutable first_time = true
    
  method scan_value value_t = 
    if Value.is_function value_t then
     let name = Name.simple value_t.val_name
     and typ = mystring_of_type_expr module_name value_t.val_type 
     and code = get_code value_t.val_code 
     and tests = match value_t.val_info with
          Some(info) -> get_test_function info.i_custom
          | _ -> []
      in
       let cfun = new cfunction name typ code in
         cfun#set_tests tests;
         set_required_code (cfun :> ccode) code_elements;
         code_elements <- code_elements @ [Function(cfun)]
    else
      let code = get_code value_t.val_code in
        let ccode = new ccode code in
          set_required_code ccode code_elements;
          code_elements <- code_elements @ [Code(ccode)]
      
  method scan_exception exception_t = 
    let cname = Name.simple exception_t.ex_name
    and ty_el = mystring_of_type_list module_name " * " exception_t.ex_args
    and code = get_code exception_t.ex_code in
      let cexc = new cexception cname ty_el code in
        set_required_code (cexc :> ccode) code_elements;
        code_elements <- code_elements @ [Exception(cexc)]
  
  method scan_type type_t = 
    let name = Name.simple type_t.ty_name
    and variant = is_variant type_t
    and code = Printf.sprintf "type %s" (get_code type_t.ty_code) in
      let ctyp = new ctype name code ~variant:variant in
        set_required_code (ctyp :> ccode) code_elements;
        code_elements <- code_elements @ [Type(ctyp)]
    
  method scan_module_type module_type_t = 
    let name = Name.simple module_type_t.mt_name
    and mt_type = match module_type_t.mt_type with Some(a) -> a | _ -> assert false in 
    let code = Printf.sprintf "module type %s = \n" 
        (Odoc_info.string_of_module_type ~complete:true mt_type) in
      let ctyp = new ctype name code ~module_signature:true in
        set_required_code (ctyp :> ccode) code_elements;
        code_elements <- code_elements @ [Type(ctyp)]
      
			(* rifare *)
(*
  method scan_module module_t = 
		let name = Name.simple module_t.m_name in
    if module_text_only module_t && name <> "Snippet_data" then 
      let name = Name.simple module_t.m_name
      and code = get_code module_t.m_code 
      and alias = try
                    get_module_alias ( (get_module_info module_t).i_custom )
                  with Not_found -> ""
        in
        let cmod = new cmodule name alias code in
          set_required_code (cmod :> ccode) code_elements;
          code_elements <- code_elements @ [Module(cmod)]
    else
      (  module_name <- Name.simple module_t.m_name;
         super#scan_module module_t;
         self#insert_snippet () )
*)
	
	method scan_module module_t = 
			if first_time then
				begin
					let list_module = Module.module_modules module_t in
					let f_filter t_module = 
						let name = Name.simple t_module.m_name in name = "Snippet_data" in
						let l_res = List.filter f_filter list_module in
						if l_res <> [] then
							begin
								first_time <- false;
								module_name <- "Snippet_data";
								super#scan_module (List.hd l_res);
								self#insert_snippet () ;
								first_time <- true
							end
				end 
				else 
					begin
						let name = Name.simple module_t.m_name
			      and code = get_code module_t.m_code 
      			and alias = try
            			        get_module_alias ( (get_module_info module_t).i_custom )
                  			with Not_found -> ""
        		in
        		let cmod = new cmodule name alias code in
          		set_required_code (cmod :> ccode) code_elements;
          		code_elements <- code_elements @ [Module(cmod)]	
					end
	  
  method scan_module_pre module_t = 
		let name = Name.simple module_t.m_name in
		if name = "Snippet_data" then
			begin
    try
      let info = get_module_info module_t in
				Printf.fprintf stderr "****\n\n%s\n\n***" (Odoc_info.string_of_info info);
        last_snippet_data <- Some ( {
            name = get_snippet_name info.i_custom;
            brief = get_snippet_brief info;
            version = get_snippet_version info.i_custom;
            keywords = get_snippet_keywords info.i_custom;
            licences = get_snippet_licences info.i_custom;
            is_deprecated = is_snippet_deprecated info.i_custom;
            use_example = get_snippet_example info.i_custom;
            path = get_snippet_directory info.i_custom;
            depend_list = get_snippet_depend repository info.i_custom;
						signature = get_signature module_t;
            parent_timestamp = get_snippet_extends info.i_custom;  
            authors = get_snippet_author info.i_custom  
                                    } );
        true
    with Not_found -> Printf.fprintf stderr "**\nEccomi qua\n**"; false
        | Missing_tag_exception s -> Printf.fprintf stderr "%s\n" s; false
				| EmptyResult -> assert false
		end
		else
			false

  method generate module_list =
    self#scan_module_list module_list
    
  method private insert_snippet module_t = 
		match last_snippet_data with
      Some(snippet_data) -> let snippet = create_snippet_by_data snippet_data code_elements repository in
                            (try
                              ignore ( repository#insert_snippet snippet );
                              Printf.fprintf stderr "Snippet %s inserted!\n" (snippet#get_name ());
                             with Failure("duplicate") -> 
                               Printf.fprintf stderr "Snippet %s missing needed s_extends tag\n" (snippet#get_name()));  
                              code_elements <- [];
                              last_snippet_data <- None
                            
      | _ -> ()
end
