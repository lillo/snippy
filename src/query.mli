exception InsertFailure of string
exception SelectFailure of string
exception EmptyResult
val all_fields : string
val doInsert :
  Dbi.connection ->
  string -> ?mark_fun:(int -> string) -> Dbi.sql_t list -> int64
val doSelectAll :
  < execute : 'a -> 'b; fetchall : unit -> 'c list; .. > -> 'a -> 'c list
val doSelectAllWithMap :
  Dbi.statement -> Dbi.sql_t list -> (Dbi.sql_t list -> 'a) -> 'a list
val doSelect1 :
  < execute : 'a -> 'b; fetchall : unit -> 'c list; .. > -> 'a -> 'c
val prepareSelect :
  Dbi.connection -> string -> string -> string -> Dbi.statement
val getElements :
  Dbi.connection ->
  string -> string -> Dbi.sql_t list -> (Dbi.sql_t list -> 'a) -> 'a list
