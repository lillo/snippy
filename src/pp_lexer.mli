val delete_ends : string -> string
val get_dir_snippet_name : string -> string * string
val get_snippet :
  < get_snippet_by_path : string -> string -> ('a -> bool) -> 'b; .. > ->
  string -> 'b
val snippet_list : Snippet.snippet list ref
val __ocaml_lex_tables : Lexing.lex_tables
val analyse :
  < get_snippet_by_path : string -> string -> ('a -> bool) -> Snippet.snippet;
    .. > ->
  Lexing.lexbuf -> Snippet.snippet list
val __ocaml_lex_analyse_rec :
  < get_snippet_by_path : string -> string -> ('a -> bool) -> Snippet.snippet;
    .. > ->
  Lexing.lexbuf -> int -> Snippet.snippet list
