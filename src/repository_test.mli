module Db :
  sig
    class connection :
      ?host:string ->
      ?port:string ->
      ?user:string ->
      ?password:string ->
      string ->
      object
        method close : unit -> unit
        method closed : bool
        method commit : unit -> unit
        method database : string
        method database_type : string
        method databases : string list
        method debug : bool
        method ex : string -> Dbi.sql_t list -> Dbi.statement
        method host : string option
        method id : int
        method password : string option
        method ping : unit -> bool
        method port : string option
        method prepare : string -> Dbi.statement
        method prepare_cached : string -> Dbi.statement
        method register_postrollback :
          (unit -> unit) -> Dbi.postrollback_handle
        method register_precommit : (unit -> unit) -> Dbi.precommit_handle
        method rollback : unit -> unit
        method set_debug : bool -> unit
        method tables : string list
        method unregister_postrollback : Dbi.postrollback_handle -> unit
        method unregister_precommit : Dbi.precommit_handle -> unit
        method user : string option
      end
    val connect :
      ?host:string ->
      ?port:string ->
      ?user:string -> ?password:string -> string -> connection
    val close : connection -> unit
    val closed : connection -> bool
    val commit : connection -> unit
    val ping : connection -> bool
    val rollback : connection -> unit
  end
val me : Snippet.author
val createListSnippet : unit -> Snippet.snippet
val createPointSnippet : unit -> Snippet.snippet
val createPolygonSnippet :
  Repository.db_repository -> int64 -> int64 -> Snippet.snippet
val main : unit -> unit
