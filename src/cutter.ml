open Repository
open Snippets_gen
open Config
open Odoc_info
open Odoc_info.Args

let tree_root = ref(Tree.root)

let rec insert_into_tree = function
	| h::t -> tree_root := Tree.add_node !tree_root (h#get_path()) h;
						insert_into_tree t
	|_ -> () 

let module_snippet repo = 
	Printf.sprintf "%s%s" (Tree.walk2 repo !tree_root) (Tree.walk repo !tree_root)

let create_module_snippet repo = 
	let s_list = Tree.depend_closure repo !Lexer_cutter.snippet_list in
		Lexer_cutter.reset_snippet_list ();
		insert_into_tree s_list;
		(module_snippet repo)
		
let create_preamble () = 
	let make_str res_tmp str = 
		Printf.sprintf "%s\n%s" res_tmp str
	in
		(List.fold_left make_str "" (!Lexer_cutter.preamble_list))
 
let create_module_preamble preamble = 
	Printf.sprintf "module Preamble = \nstruct\n\n %s\n\nend" preamble

let ocamldoc_invisible_area snipmod preamble =
	Printf.sprintf "(** module for ocamldoc*)\n(**/**)\n%s\n%s\n(**/**)" snipmod (create_module_preamble preamble)


let create_module_data data = 
		Printf.sprintf "module Snippet_data = struct \n\nopen Preamble\n\n %s \n\nend" data


let write_on_file out_channel header data = 
  output_string out_channel (header ^ "\n");
  output_string out_channel (create_module_data (data ^ "\n"));
  close_out out_channel

let error_msg file msg = 
  Printf.fprintf stderr "Error while extract snippets from %s:\n\t%s\n" file msg
  
let analyse_files (repo : db_repository) file_list = 
  let source_list = List.map (fun f -> Impl_file f) file_list in
    Odoc_info.Args.keep_code := true;
    let module_list = Odoc_info.analyse_files source_list 
    and generator = new snippet_gen repo in
      generator#generate module_list
    
let extract_snippets (repo : db_repository) file = 
  try
      let lexbuf = Lexing.from_channel (open_in file) 
      and tmp_file_list = ref [] 
			and preamble = ref (create_preamble () ) in
        let rec loop (header,data) = 
          if (header, data) <> ("","") then
            begin
              let (tmp_file_name, tmp_channel) = Filename.open_temp_file "snippet" ".ml" in
								let invisible_area = ocamldoc_invisible_area (create_module_snippet repo) (!preamble) in
								let header = Printf.sprintf "%s\n%s" invisible_area header in (* swap header and invisible_area*)
                write_on_file tmp_channel header data;
                tmp_file_list := !tmp_file_list @ [ tmp_file_name ];
								preamble := (create_preamble ());
                loop ( Lexer_cutter.next_snippet repo lexbuf )  
								
            end
        in
		Lexer_cutter.reset_preamble_list ();		
		tree_root := Tree.root;
    loop (Lexer_cutter.next_snippet repo lexbuf);
    analyse_files repo !tmp_file_list
  with Sys_error(msg) -> error_msg file msg
    
let usage_msg = Printf.sprintf "Usage:\n\n%s [db_options] file1.ml file2.ml ... fileN.ml\n\nWhere db_option are:" Sys.argv.(0)
    
let main () = 
  Config.read_config_file ();
  let file_list = ref([]) in
    let add_file file = file_list := !file_list @ [file] in
      Arg.parse Config.db_config_arg_list add_file usage_msg;
      let repo = new db_repository (Config.get_default_connection () ) in
        List.iter (extract_snippets repo) !file_list
    
let _ = main ()
    
    