exception Missing_tag_exception of string
val trim : string -> string
val throw_missing_tag_exception : string -> unit -> 'a
val get_snippet_brief : Odoc_info.info -> string
val get_author :
  < get_author_by_nickname : string -> Snippet.author; .. > ->
  string * string * string -> Snippet.author
val get_single_custom_tag :
  'a -> ('a * Odoc_info.text) list -> (string -> 'b) -> (unit -> 'b) -> 'b
val get_multi_custom_tag :
  'a ->
  ('a * Odoc_info.text) list -> (string list -> 'b) -> (unit -> 'b) -> 'b
val get_single_optional_custom_tag :
  'a -> ('a * Odoc_info.text) list -> string option
val get_single_no_optional_custom_tag :
  string -> (string * Odoc_info.text) list -> string
val get_snippet_name : (string * Odoc_info.text) list -> string
val get_snippet_version : (string * Odoc_info.text) list -> string option
val get_snippet_directory : (string * Odoc_info.text) list -> string
val get_snippet_example : (string * Odoc_info.text) list -> string option
val get_snippet_keywords :
  (string * Odoc_info.text) list -> string list option
val get_snippet_licences :
  (string * Odoc_info.text) list -> string list option
val get_snippet_author :
  (string * Odoc_info.text) list -> (string * string * string) list
val is_snippet_deprecated : (string * Odoc_info.text) list -> bool
val get_snippet_id_from_path : Repository.db_repository -> string -> int64
val get_snippet_depend :
  Repository.db_repository -> (string * Odoc_info.text) list -> int64 list
val string_datetime2dbi_date_time : string -> Dbi.date * Dbi.time
val get_snippet_extends :
  (string * Odoc_info.text) list -> (Dbi.date * Dbi.time) option
val get_module_alias : (string * Odoc_info.text) list -> string
val get_value_info : Odoc_info.Value.t_value -> Odoc_info.info
val get_test_function :
  (string * Odoc_info.text) list -> (string * string) list
val get_code : 'a option -> 'a
val get_location_element : Odoc_info.location -> string * int
val code2ccode : Snippet.code -> Snippet.ccode
val set_required_code : Snippet.ccode -> 'a -> unit
val get_module_path : Odoc_info.Module.t_module -> string
val extract_module : string -> string
val rename_sig_mod : string -> string -> string
val my_run : string -> string -> string
val get_signature : Odoc_info.Module.t_module -> string
val module_text_only : Odoc_info.Module.t_module -> bool
val is_variant : Odoc_info.Type.t_type -> bool
val is_abstract : Odoc_info.Type.t_type -> bool
type snippet_data = {
  name : string;
  brief : string;
  version : string option;
  keywords : string list option;
  licences : string list option;
  use_example : string option;
  is_deprecated : bool;
  path : string;
  parent_timestamp : Dbi.datetime option;
  authors : (string * string * string) list;
  signature : string;
  depend_list : int64 list;
}
val create_snippet_by_data :
  snippet_data ->
  Snippet.code list -> Repository.db_repository -> Snippet.snippet
val delete_module_name : string -> string -> string
val mystring_of_type_expr : string -> Types.type_expr -> string
val mystring_of_type_list :
  string -> string -> Types.type_expr list -> string
val get_module_info : Odoc_info.Module.t_module -> Odoc_info.info
class snippet_gen :
  Repository.db_repository ->
  object
    val mutable code_elements : Snippet.code list
    val mutable first_time : bool
    val mutable last_snippet_data : snippet_data option
    val mutable module_name : string
    method generate : Odoc_info.Module.t_module list -> unit
    method private insert_snippet : unit -> unit
    method scan_attribute : Odoc_info.Value.t_attribute -> unit
    method scan_class : Odoc_info.Class.t_class -> unit
    method scan_class_comment : Odoc_info.text -> unit
    method scan_class_elements : Odoc_info.Class.t_class -> unit
    method scan_class_pre : Odoc_info.Class.t_class -> bool
    method scan_class_type : Odoc_info.Class.t_class_type -> unit
    method scan_class_type_comment : Odoc_info.text -> unit
    method scan_class_type_elements : Odoc_info.Class.t_class_type -> unit
    method scan_class_type_pre : Odoc_info.Class.t_class_type -> bool
    method scan_exception : Odoc_info.Exception.t_exception -> unit
    method scan_included_module : Odoc_info.Module.included_module -> unit
    method scan_method : Odoc_info.Value.t_method -> unit
    method scan_module : Odoc_info.Module.t_module -> unit
    method scan_module_comment : Odoc_info.text -> unit
    method scan_module_elements : Odoc_info.Module.t_module -> unit
    method scan_module_list : Odoc_info.Module.t_module list -> unit
    method scan_module_pre : Odoc_info.Module.t_module -> bool
    method scan_module_type : Odoc_info.Module.t_module_type -> unit
    method scan_module_type_comment : Odoc_info.text -> unit
    method scan_module_type_elements : Odoc_info.Module.t_module_type -> unit
    method scan_module_type_pre : Odoc_info.Module.t_module_type -> bool
    method scan_type : Odoc_info.Type.t_type -> unit
    method scan_value : Odoc_info.Value.t_value -> unit
  end
