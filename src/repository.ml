open Snippet
open Query

exception Dbi_match_failure of string

let int64neg int64 = 
  if Int64.compare int64 (Int64.of_int(-1)) <= 0 then
    `Null
  else
    `Int64(int64)
    
let extract_int64 = function
    `Int64 num -> num
  | `Null -> Int64.of_int(-1)
    
let extract_bool = function
    `Int b -> if b = 0 then false else true
   |`Bool b -> b   

let create_license = function
    [`String license;`Int64 _] -> license
  | result -> raise (Dbi_match_failure (Dbi.sdebug result))
    
let create_keyword = function 
	[`String word;`Int64 snippet_id] -> word
	| result -> raise (Dbi_match_failure (Dbi.sdebug result))
    
let create_author = function
    [`Int64 id; `String nickname;`String flname;`String email] ->
      new author ~id:id nickname flname email
  | result -> raise (Dbi_match_failure (Dbi.sdebug result))

let create_code = function
    [`Int64 id;`Int64 snippet_id;`String text;`String minfo] ->
	  new ccode ~id:id text
    | result -> raise (Dbi_match_failure (Dbi.sdebug result))

let create_function = function 
    ([`Int64 id;`String name;`String ftype],text) -> new cfunction ~id:id name ftype text
  | ([`Int64 id;`String name;`String ftype;
      `Int64 i;`Int64 snippet_id;`String  txt;`String minfo],t) ->
      new cfunction ~id:id name ftype txt
  | (result,_) -> raise (Dbi_match_failure (Dbi.sdebug result))
    
let create_function_tests = function
	[`Int64 f_id;`String input;`String output] -> (input,output)
  | result -> raise (Dbi_match_failure (Dbi.sdebug result))

let create_exception = function
    ([`Int64 id;`String cname;`String ctype],text) -> new cexception ~id:id cname ctype text
  | ([`Int64 id;`String cname; `String ctype; 
      `Int64 i; `Int64 snippert_id;`String txt;`String minfo],_) ->
      new cexception ~id:id cname ctype txt
  | (result,_) -> raise (Dbi_match_failure (Dbi.sdebug result))

let create_type = function
    ([`Int64 id;`String name;`Bool polimorphic;`Bool variant;`Bool module_signature; `Bool class_type], text) ->
      (new ctype ~id:id name ~polimorphic:polimorphic ~variant:variant 
	~module_signature: module_signature ~class_type: class_type text )
  |([`Int64 id;`String name;`Int polimorphic;`Int variant;`Int module_signature; `Int class_type],text) ->
      (new ctype ~id:id name ~polimorphic:(if polimorphic = 0 then false else true) 
	 ~variant:(if variant = 0 then false else true)
	~module_signature:(if module_signature = 0 then false else true) 
    ~class_type:(if class_type = 0 then false else true) text )
  | ([`Int64 id;`String name;`Bool polimorphic;`Bool variant;`Bool module_signature;`Bool class_type;
      `Int64 i;`Int64 snippet_id;`String txt;`String minfo],_) ->
      (new ctype ~id:id name ~polimorphic:polimorphic ~variant:variant 
	~module_signature:module_signature ~class_type:class_type txt)
  | ([`Int64 id;`String name;`Int polimorphic;`Int variant;`Int module_signature; `Int class_type;
      `Int64 i;`Int64 snippet_id;`String txt;`String minfo],_) ->
      (new ctype ~id:id name ~polimorphic:(if polimorphic = 0 then false else true) 
	 ~variant:(if variant = 0 then false else true)
	 ~module_signature:(if module_signature = 0 then false else true) 
     ~class_type:(if class_type = 0 then false else true)   txt )
  | (result,_) -> raise (Dbi_match_failure (Dbi.sdebug result))

let create_class = function
    ([`Int64 id;`String name;`Bool is_virtual], text) ->
      new cclass ~id:id name ~is_virtual:is_virtual text
  | ([`Int64 id;`String name;`Int is_virtual],text) ->
      new cclass ~id:id name ~is_virtual:(if is_virtual = 0 then false else true)
	 text
  | ([`Int64 id;`String name;`Bool is_virtual; 
      `Int64 i;`Int64 snippet_id;`String txt; `String minfo],_) ->
      new cclass ~id:id name ~is_virtual:is_virtual txt
  | ([`Int64 id;`String name;`Int is_virtual;
      `Int64 i;`String txt; `String minfo;`Int64 snippet_id],_) ->
      new cclass ~id:id name ~is_virtual:(if is_virtual = 0 then false else true) txt
  | (result,_) -> raise (Dbi_match_failure (Dbi.sdebug result))

let create_module = function
    ([`Int64 id;`String name;`String alias],text) -> 
      new cmodule ~id:id name alias text
  | ([`Int64 id;`String name;`String alias;
      `Int64 i; `Int64 snippet_id;`String text;`String minfo],_) ->
      new cmodule ~id:id name alias text
  | (result,_) -> raise (Dbi_match_failure (Dbi.sdebug result))


let get_element_by_snippet_id  dbh table where_exp (snippet_id:int64) create_element = 
    getElements dbh table where_exp [`Int64 snippet_id] create_element

let get_dir_by_name_parent dbh name parent = 
let p = int64neg parent in
let where =  Printf.sprintf "d.name = ? AND d.parent %s ?"
  (if p = `Null then "IS" else "=") 
  in
    doSelect1 (
      prepareSelect dbh all_fields "Directories d" where
            ) [`String name;(int64neg parent)]
  
let get_dir_by_id dbh id = 
  doSelect1 (
    prepareSelect dbh all_fields "Directories d" "d.id = ?"
  ) [`Int64 id]
  
let insert_dir dbh name parent = 
  doInsert dbh "Directories(name,parent)" [`String name; int64neg parent]
  
let insert_into_snippet_table dbh (s:snippet) id_dir = 
  doInsert dbh "Snippets(name,brief,version,deprecated,useExample,idDirectory,parent,snippetTree,signature)"
    [`String (s#get_name()) ; `String (s#get_brief()); `String (s#get_version()); `Bool (s#is_deprecated());
      `String (s#get_use_example()); `Int64 id_dir; int64neg (s#get_parent()) ; int64neg (s#get_snippet_tree());
			`String (s#get_signature())]

let walk_path f_res dbh ?parent path  = 
  let p = match parent with
      Some a -> a | None -> Int64.of_int(-1)
  and regexp = Str.regexp "/" in
  let dir_list = Str.split regexp path in
    let f_aux p dir = 
      try
	    (match get_dir_by_name_parent dbh dir p with
	       [`Int64 id; _ ; _] -> id
	     | result -> raise (Dbi_match_failure (Dbi.sdebug result)))
      with EmptyResult -> f_res dbh dir p
    in
      List.fold_left f_aux p dir_list

let insert_path = walk_path insert_dir
  
let get_id_dir = 
  let f_aux d dir p = raise (Failure "Error directory non exists") in
    walk_path f_aux
  
  
let rec remake_path dbh id = 
  match get_dir_by_id dbh id with
      [`Int64 i; `String name; `Int64 parent] ->
        Printf.sprintf "%s/%s" (remake_path dbh parent) name
    | [`Int64 i; `String name; `Null] ->
        name
    | [] -> ""
    | result -> raise (Dbi_match_failure (Dbi.sdebug result))  
  
let get_data_specific_snippet fields dbh name dir creation_data =
  doSelect1 (
    prepareSelect dbh fields "Snippets s" "s.name = ? AND s.idDirectory = ? AND s.creationData = ?"
            )  [`String name; `Int64 dir; `Timestamp creation_data]
  
let get_specific_snippet dbh name dir creation_data =
  get_data_specific_snippet all_fields

let get_snippet_with_parent dbh name dir parent= 
let where = Printf.sprintf "s.name = ? AND s.idDirectory = ? AND s.parent %s ?" 
  (if parent = `Null then "IS" else "=") in
  let query_res = doSelect1 (
              prepareSelect dbh "id" "Snippets s" where
                            ) [`String name; `Int64 dir; parent]
  in
    match query_res with
        [`Int64 id] -> id
      | result -> raise (Dbi_match_failure (Dbi.sdebug result))
        
let get_snippet_tree_root dbh name dir =
  get_snippet_with_parent dbh name dir `Null
  
let get_snippet_tree dbh id_root = 
  doSelectAll (
    prepareSelect dbh all_fields "Snippets s" "s.snippetTree = ?"
           ) [`Int64 id_root]
        
let duplicate_root dbh (s:snippet) dir = 
  try
    let _= get_snippet_tree_root dbh (s#get_name ()) dir in
      true && (int64neg (s#get_parent ())) = `Null
  with EmptyResult -> false
    
let  double_placeholders n =
  let rec f_aux num =
    if num > 0 then
      (Dbi.placeholders 2) ^ (if num-1 = 0 then "" else ",") ^ f_aux (num-1)
    else
      ""
  in
    f_aux (n/2)
  
let concat_snippet_id id_snippet depend_list =
  let rlist = ref([])
  in
    List.iter(
      function p -> rlist := !rlist @ [`Int64 id_snippet; `Int64 p]
    ) depend_list;
    !rlist
    
let insert_dependencies dbh id_snippet depend_list = 
  if depend_list <> [] then
    let param_list = concat_snippet_id id_snippet depend_list in
      ignore ( 
        doInsert dbh "Dependencies(idSnippet,idSnippetTree)" ~mark_fun:double_placeholders 
          param_list 
      )
        
let get_required_snippet_tree dbh snippet_id = 
  let map_function = function
        [`Int64 id] -> id
      | result -> raise (Dbi_match_failure (Dbi.sdebug result))
        in
        doSelectAllWithMap (
          prepareSelect dbh "idSnippetTree" "Dependencies d" "d.idSnippet = ?"
                         ) [`Int64 snippet_id] map_function
  
let get_snippet_path dbh snippet_id = 
	let res = doSelect1 (
			prepareSelect dbh "name, idDirectory" "Snippets s" "s.id = ?"
					) [`Int64 snippet_id]
	in                        
	match res with
		| [`String name;`Int64 id_dir] -> Printf.sprintf "%s/%s" (remake_path dbh id_dir) name
		| result -> raise (Dbi_match_failure(Dbi.sdebug result))

(*********)

class db_repository (dbh: Dbi.connection) = 
object(self)
     
  method private insert_code (code:ccode) (minfo:string) (snippet_id: int64) =
    let id_code = doInsert dbh "Code(text,metainfo,idSnippet)"  
      [`String (code#get_text()); `String minfo; `Int64 snippet_id];
    in
      Printf.fprintf stderr "insertCode: insert code: %s\n" (code#get_text());
      code#set_id id_code;
      id_code

  method private insert_function_tests (idFunction: int64) test_list = 
    List.iter (
      fun(input,output) -> let _= doInsert dbh "FunctionTests(idFunction,input,output)"
	[`Int64 idFunction; `String input; `String output] in ()) test_list

  method private insert_function (code:cfunction) (snippet_id:int64) =
    let id_code = self#insert_code (code:>ccode) "Functions" snippet_id in
      Printf.printf "insertFunction: insert function part\n!";
      ignore(doInsert dbh "Functions(id,name,f_type)" 
	[`Int64 id_code;`String (code#get_name());`String (code#get_type())]);
      self#insert_function_tests id_code (code#get_tests());
      id_code
     

  method private insert_exception (code:cexception) (snippet_id:int64) =
    let id_code = self#insert_code (code:>ccode) "Exceptions" snippet_id in
      doInsert dbh "Exceptions(id,constructorName,constructorType)" 
	[`Int64 id_code; `String (code#get_constructor_name()); 
	 `String (code#get_constructor_type())]

  method private insert_type (code:ctype) (snippet_id:int64) =
    let idCode = self#insert_code (code:>ccode) "Types" snippet_id in
      doInsert dbh "Types(id,name,polimorphic,variant,moduleSignature,classType)"
	[`Int64 idCode; `String (code#get_name()); `Bool (code#is_polimorphic ());
	 `Bool (code#is_variant()); `Bool (code#is_module_signature ()); `Bool (code#is_class_type ())]

  method private insert_class (code:cclass) (snippet_id:int64) =
    let id_code = self#insert_code (code:>ccode) "Classes" snippet_id in
      doInsert dbh "Classes(id,name,isVirtual)"
	[`Int64 id_code; `String (code#get_name());`Bool (code#is_virtual ())]

  method private insert_module (code:cmodule) (snippet_id:int64) =
    let id_code = self#insert_code (code:>ccode) "Modules" snippet_id in
      doInsert dbh "Modules(id,name,alias)"
	[`Int64 id_code;`String (code#get_name()); `String (code#get_alias())]


  method private insert_code_element code_element snippet_id =
    match code_element with 
      Code(cc) -> self#insert_code cc "Code" snippet_id 
    | Function(cf) -> self#insert_function cf snippet_id
    | Exception(ce) -> self#insert_exception ce snippet_id
    | Class(ccl) -> self#insert_class ccl snippet_id
    | Type(ct) -> self#insert_type ct snippet_id
    | Module(cm) -> self#insert_module cm snippet_id

  method private insert_code_element_list code_list snippet_id =
    let f_aux element = 
      let _ = self#insert_code_element element snippet_id in ()
    in
      List.iter f_aux code_list
	
  method insert_author (a:author) = 
    doInsert dbh "Authors(nickname,nameLastName,email)"
      [`String (a#get_nickname());`String (a#get_name_last_name()); 
       `String (a#get_email ())]

  method private insert_keyword (word:string) (snippet_id:int64) = 
    doInsert dbh "Keywords(keyword,idSnippet)"
      [`String word; `Int64 snippet_id]

  method private insert_keyword_list (word_list:string list) (snippet_id:int64) =
    let f_aux word = 
      let _= self#insert_keyword word snippet_id in ()
    in
      List.iter f_aux word_list

  method private insert_license (license:string) (snippet_id:int64) = 
    doInsert dbh "Licenses(license, idSnippet)"
      [`String license; `Int64 snippet_id]

  method private insert_license_list (licence_list:string list) (snippet_id:int64) =
    let f_aux licence =
      let _= self#insert_license licence snippet_id in ()
    in
     List.iter f_aux licence_list
	
  method private insert_author_snippet (snippet_id:int64) (author:int64) = 
    doInsert dbh "SnippetsAuthors(idSnippet,idAuthor)" 
      [`Int64 snippet_id;`Int64 author]

  method insert_snippet (s:snippet) = 
   try
     let path_id = insert_path dbh (s#get_path ())
     in
       if duplicate_root dbh s path_id then
         raise (Failure("duplicate"));
       let id_snippet = insert_into_snippet_table dbh s path_id in
         self#insert_keyword_list (s#get_keywords ()) id_snippet;
         self#insert_license_list (s#get_licenses ()) id_snippet;
         self#insert_code_element_list (s#get_code ()) id_snippet;
         List.iter (
           function (a:author) ->
               let id_author = 
                 try
                   (self#get_author_by_nickname (a#get_nickname()))#get_id()
                 with EmptyResult -> self#insert_author a
               in
               ignore( self#insert_author_snippet id_snippet id_author )
                   ) (s#get_authors());
         insert_dependencies dbh id_snippet (s#get_depend_list ());
         dbh#commit ();
         id_snippet
   with a -> dbh#rollback(); raise a  (*(Failure("insert_snippet"))*)
        
     
  method private get_function_test function_id = 
    let create_element = function
	[`String input;`String output;`Int64 f_id] -> (input,output)
    | result -> raise (Dbi_match_failure (Dbi.sdebug result))
    in
      getElements dbh "FunctionTests ft" "ft.idFunction = ?"
	[`Int64 function_id] create_element
	

  method private get_element_code_by_id el_table id text = 
    let (table,where) = match text with
	true -> Printf.sprintf "%s el, Code c" el_table, "el.id = c.id AND el.id = ?"
      | false -> Printf.sprintf "%s el" el_table , "el.id = ?"
    in
      Printf.fprintf stderr "getElementCodeById(): tables_expr: %s -- where_expr: %s\n" table where;
      doSelect1 (
	prepareSelect dbh all_fields table where
      ) [`Int64 id]


  method private get_code_by_id code_id = 
    create_code (self#get_element_code_by_id "Code" code_id false)
	
  method private get_function_by_id ?hasText id= 
    let text = match hasText with
	None -> ""
      | Some a -> a
    in
    let f = create_function ((self#get_element_code_by_id "Functions" id (text="")), text)
    in
      f#set_tests (self#get_function_test id);
      f

  method private get_exception_by_id ?hasText id = 
    let text = match hasText with
	None -> ""
      | Some a -> a
    in
    create_exception ((self#get_element_code_by_id "Exceptions" id (text="")), text)
    
  method private get_type_by_id ?hasText id = 
    let text = match hasText with
	None -> ""
      | Some a -> a
    in
      create_type ((self#get_element_code_by_id "Types" id (text="")), text)
  

  method private get_class_by_id ?hasText id = 
    let text = match hasText with
	None -> ""
      | Some a -> a
    in
      create_class ((self#get_element_code_by_id "Classes" id (text="")),text)

  method private get_module_by_id ?hasText id =
    let text = match hasText with
	None -> ""
      | Some a -> a
    in
      create_module ((self#get_element_code_by_id "Modules" id (text="")), text)


  method private get_licenses_by_snippet_id snippet_id = 
    get_element_by_snippet_id dbh "Licenses l" "l.idSnippet = ?"
      snippet_id create_license

  method private get_keywords_by_snippet_id snippet_id = 
    get_element_by_snippet_id dbh "Keywords k" "k.idSnippet = ?"
	  snippet_id create_keyword

  method private get_codes_by_snippet_id (snippet_id:int64) = 
    let create_element = 
      function [`Int64 id;`String text;`String minfo;`Int64 snippet_id] ->
	(
	match minfo with
	    "Functions" -> Function(self#get_function_by_id ~hasText:text id)
	  | "Exceptions" -> Exception(self#get_exception_by_id ~hasText:text id)
	  | "Classes" -> Class(self#get_class_by_id ~hasText:text id)
	  | "Types" -> Type(self#get_type_by_id ~hasText:text id)
	  | "Modules" -> Module(self#get_module_by_id ~hasText:text id)
	  | _ -> Code(self#get_code_by_id id)
	)
   | result -> raise (Dbi_match_failure (Dbi.sdebug result))
    in
      get_element_by_snippet_id  dbh "Code code" "code.idSnippet = ?" 
	snippet_id create_element

  method private get_authors_by_snippet_id snippet_id = 
    let createElement = function
	[`Int64 id;`String nick; `String flname;`String email;`Int64 s;`Int64 s1] -> 
	   new author ~id:id nick flname email
      | _ -> assert false
    in
      get_element_by_snippet_id dbh "Authors a,SnippetsAuthors s"
	"s.idAuthor = a.id AND s.idSnippet = ?" snippet_id
	createElement
  (* deprecated *)
  method get_snippets_by_name name = 
    failwith "This method need to change";
    getElements dbh "Snippets s" "s.name = ?" [`String name]
      self#create_snippet
  (* deprecated *)
  method get_snippets_by_brief word = 
    failwith "This method need to change";
    getElements dbh "Snippets s" "s.brief LIKE \'%?%\'" [`String word]
      self#create_snippet
  (* deprecated *)
  method get_snippets_by_keywords keywordslist = 
    failwith "This method need to change";
    let qparams = List.map (
      function w -> `String w
    )keywordslist
    in
    getElements dbh "Snippets s, Keywords k"
      (Printf.sprintf "s.id = k.idSnippet AND k.keyword IN %s" (Dbi.placeholders (List.length qparams)))
      qparams
      self#create_snippet

  method private get_element_code_by_name element name create_element= 
    let (tbl, where_expr) = Printf.sprintf "%s el, Code code" element, 
      Printf.sprintf "el.id = code.id AND el.%s = ?" (if element = "Exceptions" then
				  "constructorName"
				else
				  "name")
    in
      getElements dbh tbl where_expr [`String name]
	create_element
	

  method get_function_by_name name = 
    let create el = 
      let f = create_function (el,"") 
      in
	f#set_tests (self#get_function_test (f#get_id())); 
	Function(f)
    in
      self#get_element_code_by_name "Functions" name create
      

  method get_exception_by_constructor_name name = 
    let create el = Exception(create_exception (el,""))
    in
      self#get_element_code_by_name "Exceptions" name create

  method get_type_by_name name = 
    let create el = Type(create_type (el,""))
    in
      self#get_element_code_by_name "Types" name create

  method get_class_by_name name = 
    let create el = Class(create_class (el,""))
    in
      self#get_element_code_by_name "Classes" name create

  method get_module_by_name name = 
    let create el = Module(create_module (el,""))
    in
      self#get_element_code_by_name "Modules" name create

  method private get_author_by_id id = 
    match 
      getElements dbh "Authors a" "a.id = ?" [`Int64 id] create_author
    with
	h::t -> h
      | [] -> raise EmptyResult

  method get_author_by_nickname nickname = 
    match
      getElements dbh "Authors a" "a.nickname = ?" 
	[`String nickname] create_author
    with
	h::t -> h
      | [] -> raise EmptyResult
    
  method get_snippet_tree_root_id name path = 
    try
      let id_dir = get_id_dir dbh path in
        get_snippet_tree_root dbh name id_dir 
    with Failure(a) -> raise EmptyResult
      

  method get_specific_snippet_id name path creation_data = 
    try
      let id_dir = get_id_dir dbh path in
        match get_data_specific_snippet "id" dbh name id_dir creation_data with
          [`Int64 id] -> id
        | result -> raise (Dbi_match_failure (Dbi.sdebug result)) 
    with Failure(s) -> raise EmptyResult    
      
  method private create_snippet (snippet_data:Dbi.sql_t list) = 
    match snippet_data with
      [`Int64 id; `Int64 id_dir; (`Int64 _ | `Null) as snippet_tree; (`Int64 _ | `Null) as parent;
       `String name; `String brief; `String version; (`Bool _ | `Int _) as deprecated; 
       `String use_example; `Timestamp ts ; `String signature] ->
         let path = remake_path dbh id_dir 
         and keywords = self#get_keywords_by_snippet_id id
         and licenses = self#get_licenses_by_snippet_id id
         and authors = self#get_authors_by_snippet_id id
         and code_list = self#get_codes_by_snippet_id id
         and depend_clos = get_required_snippet_tree dbh id
         in
         new snippet ~id:id ~parent:(extract_int64 parent) ~snippet_tree:(extract_int64 snippet_tree)
           ~deprecated:(extract_bool deprecated) name brief version keywords licenses use_example 
           path signature authors depend_clos code_list
    | result -> raise (Dbi_match_failure (Dbi.sdebug result))
    
  method private  select_snippet_from_tree id_snippet_tree (selector: int->bool) =
    let res = doSelect1 (
      prepareSelect dbh all_fields "Snippets s" "s.id = ? OR s.snippetTree = ? ORDER BY s.creationData DESC"
              ) [`Int64 id_snippet_tree; `Int64 id_snippet_tree]
    in
      self#create_snippet res
    
  method private get_clos_depend snippet_id (selector: int->bool)= 
    let d_list = get_required_snippet_tree dbh snippet_id 
    and r_list = ref([]) in
    List.iter (function id_snippet_tree -> 
      r_list := !r_list @ [self#select_snippet_from_tree id_snippet_tree selector] ) d_list;
    !r_list
    
  method get_snippet_by_id id_snippet = 
    let res = doSelect1 (
      prepareSelect dbh all_fields "Snippets s" "s.id = ?"
                        )[`Int64 id_snippet]
    in
    self#create_snippet res
    
  method get_depend_closure (snippet:snippet) (selector: int -> bool)= 
    if (int64neg (snippet#get_id())) = `Null then
      raise (Failure("Invalid snippet"))
    else
      List.rev (self#get_clos_depend (snippet#get_id()) selector)
    
  method get_snippet_by_path name path (selector : int -> bool) =
    try
      let id_dir = get_id_dir dbh path in
      let id_snippet_tree = get_snippet_tree_root dbh name id_dir in
        self#select_snippet_from_tree id_snippet_tree selector
    with Failure(s) -> raise EmptyResult    

	method get_path_of_depend_clos (s: snippet) =
		List.map (get_snippet_path dbh) (s#get_depend_list ())
		
end
      


