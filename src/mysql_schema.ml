(*
 * Codice creato in modo automatico dallo script make_mysql_schema.py
 *)


let delete_db_query dbname = Printf.sprintf "DROP DATABASE IF EXISTS %s" dbname
let create_db_query dbname = Printf.sprintf "CREATE DATABASE %s" dbname
let use_db_query dbname = Printf.sprintf "USE %s" dbname

let create_table_query = [


"CREATE TABLE Directories ( \
  id BIGINT NOT NULL AUTO_INCREMENT, \
  name VARCHAR(255) NOT NULL, \
  parent BIGINT NULL, \
  PRIMARY KEY(id), \
  FOREIGN KEY(parent) \
    REFERENCES Directories(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Authors ( \
  id BIGINT NOT NULL AUTO_INCREMENT, \
  nickname VARCHAR(50) NOT NULL, \
  nameLastName VARCHAR(255) NULL, \
  email VARCHAR(255) NULL, \
  PRIMARY KEY(id) \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Snippets ( \
  id BIGINT NOT NULL AUTO_INCREMENT, \
  idDirectory BIGINT NULL, \
  snippetTree BIGINT NULL, \
  parent BIGINT NULL, \
  name VARCHAR(255) NOT NULL, \
  brief VARCHAR(255) NOT NULL, \
  version VARCHAR(50) NULL, \
  deprecated BOOL NULL, \
  useExample TEXT NULL, \
  creationData TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, \
  signature TEXT NULL, \
  PRIMARY KEY(id), \
  FOREIGN KEY(parent) \
    REFERENCES Snippets(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION, \
  FOREIGN KEY(snippetTree) \
    REFERENCES Snippets(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION, \
  FOREIGN KEY(idDirectory) \
    REFERENCES Directories(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Code ( \
  id BIGINT NOT NULL AUTO_INCREMENT, \
  text TEXT NOT NULL, \
  metaInfo VARCHAR(25) NOT NULL DEFAULT \"Code\", \
  idSnippet BIGINT NOT NULL, \
  PRIMARY KEY(id), \
  FOREIGN KEY(idSnippet) \
    REFERENCES Snippets(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Functions ( \
  id BIGINT NOT NULL, \
  name VARCHAR(255) NOT NULL, \
  f_type VARCHAR(255) NOT NULL, \
  PRIMARY KEY(id), \
  FOREIGN KEY(id) \
    REFERENCES Code(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Exceptions ( \
  id BIGINT NOT NULL, \
  constructorName VARCHAR(255) NOT NULL, \
  constructorType VARCHAR(255) NULL, \
  PRIMARY KEY(id), \
  FOREIGN KEY(id) \
    REFERENCES Code(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE SnippetsAuthors ( \
  idAuthor BIGINT NOT NULL, \
  idSnippet BIGINT NOT NULL, \
  PRIMARY KEY(idAuthor, idSnippet), \
  FOREIGN KEY(idSnippet) \
    REFERENCES Snippets(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION, \
  FOREIGN KEY(idAuthor) \
    REFERENCES Authors(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Dependencies ( \
  idSnippet BIGINT NOT NULL, \
  idSnippetTree BIGINT NOT NULL, \
  PRIMARY KEY(idSnippet, idSnippetTree), \
  FOREIGN KEY(idSnippetTree) \
    REFERENCES Snippets(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION, \
  FOREIGN KEY(idSnippet) \
    REFERENCES Snippets(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Licenses ( \
  license VARCHAR(255) NOT NULL, \
  idSnippet BIGINT NOT NULL, \
  PRIMARY KEY(license, idSnippet), \
  FOREIGN KEY(idSnippet) \
    REFERENCES Snippets(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Keywords ( \
  keyword VARCHAR(50) NOT NULL, \
  idSnippet BIGINT NOT NULL, \
  PRIMARY KEY(keyword, idSnippet), \
  FOREIGN KEY(idSnippet) \
    REFERENCES Snippets(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Types ( \
  id BIGINT NOT NULL, \
  name VARCHAR(255) NOT NULL, \
  polimorphic BOOL NULL DEFAULT FALSE, \
  variant BOOL NULL DEFAULT FALSE, \
  moduleSignature BOOL NULL DEFAULT FALSE, \
  classType BOOL NULL DEFAULT FALSE, \
  PRIMARY KEY(id), \
  FOREIGN KEY(id) \
    REFERENCES Code(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Modules ( \
  id BIGINT NOT NULL, \
  name VARCHAR(255) NOT NULL, \
  alias VARCHAR(255) NULL, \
  PRIMARY KEY(id), \
  FOREIGN KEY(id) \
    REFERENCES Code(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE FunctionTests ( \
  input VARCHAR(255) NOT NULL, \
  output VARCHAR(255) NOT NULL, \
  idFunction BIGINT NOT NULL, \
  PRIMARY KEY(input, output, idFunction), \
  FOREIGN KEY(idFunction) \
    REFERENCES Functions(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;


"CREATE TABLE Classes ( \
  id BIGINT NOT NULL, \
  name VARCHAR(255) NOT NULL, \
  isVirtual BOOL NULL DEFAULT FALSE, \
  PRIMARY KEY(id), \
  FOREIGN KEY(id) \
    REFERENCES Code(id) \
      ON DELETE NO ACTION \
      ON UPDATE NO ACTION \
) \
TYPE=InnoDB"

	;



]



