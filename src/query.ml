exception InsertFailure of string
exception SelectFailure of string
exception EmptyResult

let all_fields = "*"

let doInsert (dbh:Dbi.connection) tableCol ?mark_fun data = 
    let markStr = match mark_fun with 
	Some mark_f ->  mark_f(List.length data) |
	None -> Dbi.placeholders (List.length data)
    in
    let query_str = Printf.sprintf "INSERT INTO %s VALUES %s" tableCol markStr
    in
    let q = dbh#prepare query_str
    in
      Printf.fprintf 
	stderr "doInsert: do query: %s\ndoInsert: the params number is %d\n"
	query_str (List.length data);
      (try
	 q#execute data
       with a -> raise a);
    Printf.fprintf stderr "doInsert: query success!\n";
    q#serial ""

let doSelectAll query params = 
  try
    Printf.fprintf stderr "doSelectAll: do select\n";
    query#execute params; 
    Printf.fprintf stderr "doSelectAll: select success\n";
    query#fetchall()
  with
      Not_found -> []
    | _ ->  raise ( SelectFailure "doSelectAll: select fault!" )

let doSelectAllWithMap (query: Dbi.statement) params map_function = 
  try
    Printf.fprintf stderr "doSelectAllWithMap: do select\n";
    query#execute params;
    Printf.fprintf stderr "doSelectAllWithMap: select success\n";
    query#map map_function
  with 
      Failure(str) -> raise ( SelectFailure (Printf.sprintf "doSelectAllWithMap: failure: %s\n" str))

let doSelect1 query param = 
  match (doSelectAll query param) with
      h::t -> h
    | [] -> raise EmptyResult

let prepareSelect (dbh: Dbi.connection) select_expr tables where_expr = 
  let query_str =  Printf.sprintf "SELECT %s FROM %s WHERE %s" select_expr tables where_expr in
    Printf.fprintf stderr "prepareSimpleQuery: prepare: %s\n" query_str;
    dbh#prepare query_str


let getElements dbh tables where_expr params create_element = 
  doSelectAllWithMap (
    prepareSelect dbh all_fields tables where_expr
  ) params create_element