open Repository
open Snippet
open Config
open Tree



let print_list l = 
  let f_aux s = Printf.printf "*********\n%s\n" (s#to_text()) in
    List.iter f_aux l

let usage_msg = Printf.sprintf 
  "Usage:\n\n%s [db_options] file1.ml file2.ml ... fileN.ml\n\nWhere db_option are:" Sys.argv.(0)
  
let rec extract_snippet repo = function
    [] -> []
  | h::t -> 
    let h_list = 
      try
        let lexbuf = Lexing.from_channel (open_in h )in
          Pp_lexer.analyse repo lexbuf
      with Sys_error(c) -> []    
    in
      h_list @ extract_snippet repo t  
  
let pp_files repo file_list = 
  let snippet_list = depend_closure repo (extract_snippet repo file_list) in 
    let rec make_tree tree = function
        [] -> tree
      | h::t ->  let ns = add_node tree (h#get_path()) h in make_tree ns t
    in
      let  cout = open_out "snippet.ml" in
			let tree = make_tree root snippet_list in
				output_string cout (walk2 repo tree);
        output_string cout (walk repo tree);
				output_string cout "include Snippet";
        close_out cout
        
let main () = 
  Config.read_config_file ();
  let file_list = ref([]) in
    let add_file file = file_list := !file_list @ [file] in
      Arg.parse Config.db_config_arg_list add_file usage_msg;
      let repo = new db_repository (Config.get_default_connection () ) in
        pp_files repo !file_list
  
let _= main ()