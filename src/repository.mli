exception Dbi_match_failure of string
val int64neg : Int64.t -> [> `Int64 of Int64.t | `Null ]
val extract_int64 : [< `Int64 of int64 | `Null ] -> int64
val extract_bool : [< `Bool of bool | `Int of int ] -> bool
val create_license : Dbi.sql_t list -> string
val create_keyword : Dbi.sql_t list -> string
val create_author : Dbi.sql_t list -> Snippet.author
val create_code : Dbi.sql_t list -> Snippet.ccode
val create_function : Dbi.sql_t list * string -> Snippet.cfunction
val create_function_tests : Dbi.sql_t list -> string * string
val create_exception : Dbi.sql_t list * string -> Snippet.cexception
val create_type : Dbi.sql_t list * string -> Snippet.ctype
val create_class : Dbi.sql_t list * string -> Snippet.cclass
val create_module : Dbi.sql_t list * string -> Snippet.cmodule
val get_element_by_snippet_id :
  Dbi.connection ->
  string -> string -> int64 -> (Dbi.sql_t list -> 'a) -> 'a list
val get_dir_by_name_parent :
  Dbi.connection -> string -> Int64.t -> Dbi.sql_t list
val get_dir_by_id : Dbi.connection -> int64 -> Dbi.sql_t list
val insert_dir : Dbi.connection -> string -> Int64.t -> int64
val insert_into_snippet_table :
  Dbi.connection -> Snippet.snippet -> Int64.t -> int64
val walk_path :
  (Dbi.connection -> string -> Int64.t -> Int64.t) ->
  Dbi.connection -> ?parent:Int64.t -> string -> Int64.t
val insert_path : Dbi.connection -> ?parent:Int64.t -> string -> Int64.t
val get_id_dir : Dbi.connection -> ?parent:Int64.t -> string -> Int64.t
val remake_path : Dbi.connection -> int64 -> string
val get_data_specific_snippet :
  string ->
  Dbi.connection -> string -> int64 -> Dbi.datetime -> Dbi.sql_t list
val get_specific_snippet :
  'a ->
  'b ->
  'c ->
  'd -> Dbi.connection -> string -> int64 -> Dbi.datetime -> Dbi.sql_t list
val get_snippet_with_parent :
  Dbi.connection -> string -> int64 -> Dbi.sql_t -> int64
val get_snippet_tree_root : Dbi.connection -> string -> int64 -> int64
val get_snippet_tree : Dbi.connection -> int64 -> Dbi.sql_t list list
val duplicate_root : Dbi.connection -> Snippet.snippet -> int64 -> bool
val double_placeholders : int -> string
val concat_snippet_id : 'a -> 'a list -> [> `Int64 of 'a ] list
val insert_dependencies : Dbi.connection -> int64 -> int64 list -> unit
val get_required_snippet_tree : Dbi.connection -> int64 -> int64 list
val get_snippet_path : Dbi.connection -> int64 -> string
class db_repository :
  Dbi.connection ->
  object
    method private create_snippet : Dbi.sql_t list -> Snippet.snippet
    method private get_author_by_id : int64 -> Snippet.author
    method get_author_by_nickname : string -> Snippet.author
    method private get_authors_by_snippet_id : Int64.t -> Snippet.author list
    method private get_class_by_id :
      ?hasText:string -> Int64.t -> Snippet.cclass
    method get_class_by_name : string -> Snippet.code list
    method private get_clos_depend :
      Int64.t -> (int -> bool) -> Snippet.snippet list
    method private get_code_by_id : Int64.t -> Snippet.ccode
    method private get_codes_by_snippet_id : Int64.t -> Snippet.code list
    method get_depend_closure :
      Snippet.snippet -> (int -> bool) -> Snippet.snippet list
    method private get_element_code_by_id :
      string -> Int64.t -> bool -> Dbi.sql_t list
    method private get_element_code_by_name :
      string ->
      string -> (Dbi.sql_t list -> Snippet.code) -> Snippet.code list
    method get_exception_by_constructor_name : string -> Snippet.code list
    method private get_exception_by_id :
      ?hasText:string -> Int64.t -> Snippet.cexception
    method private get_function_by_id :
      ?hasText:string -> Int64.t -> Snippet.cfunction
    method get_function_by_name : string -> Snippet.code list
    method private get_function_test : Int64.t -> (string * string) list
    method private get_keywords_by_snippet_id : Int64.t -> string list
    method private get_licenses_by_snippet_id : Int64.t -> string list
    method private get_module_by_id :
      ?hasText:string -> Int64.t -> Snippet.cmodule
    method get_module_by_name : string -> Snippet.code list
    method get_path_of_depend_clos : Snippet.snippet -> string list
    method get_snippet_by_id : int64 -> Snippet.snippet
    method get_snippet_by_path :
      string -> string -> (int -> bool) -> Snippet.snippet
    method get_snippet_tree_root_id : string -> string -> int64
    method get_snippets_by_brief : string -> Snippet.snippet list
    method get_snippets_by_keywords : string list -> Snippet.snippet list
    method get_snippets_by_name : string -> Snippet.snippet list
    method get_specific_snippet_id :
      string -> string -> Dbi.datetime -> int64
    method private get_type_by_id :
      ?hasText:string -> Int64.t -> Snippet.ctype
    method get_type_by_name : string -> Snippet.code list
    method insert_author : Snippet.author -> Int64.t
    method private insert_author_snippet : Int64.t -> Int64.t -> int64
    method private insert_class : Snippet.cclass -> int64 -> int64
    method private insert_code : Snippet.ccode -> string -> int64 -> int64
    method private insert_code_element : Snippet.code -> int64 -> int64
    method private insert_code_element_list :
      Snippet.code list -> int64 -> unit
    method private insert_exception : Snippet.cexception -> int64 -> int64
    method private insert_function : Snippet.cfunction -> int64 -> int64
    method private insert_function_tests :
      int64 -> (string * string) list -> unit
    method private insert_keyword : string -> int64 -> int64
    method private insert_keyword_list : string list -> int64 -> unit
    method private insert_license : string -> int64 -> int64
    method private insert_license_list : string list -> int64 -> unit
    method private insert_module : Snippet.cmodule -> int64 -> int64
    method insert_snippet : Snippet.snippet -> int64
    method private insert_type : Snippet.ctype -> int64 -> int64
    method private select_snippet_from_tree :
      int64 -> (int -> bool) -> Snippet.snippet
  end
