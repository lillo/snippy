val print_list : < to_text : unit -> string; .. > list -> unit
val usage_msg : string
val extract_snippet :
  < get_snippet_by_path : string -> string -> ('a -> bool) -> Snippet.snippet;
    .. > ->
  string list -> Snippet.snippet list
val pp_files :
  < get_depend_closure : Snippet.snippet ->
                         ('a -> bool) -> Snippet.snippet list;
    get_path_of_depend_clos : Snippet.snippet -> string list;
    get_snippet_by_path : string -> string -> ('b -> bool) -> Snippet.snippet;
    .. > ->
  string list -> unit
val main : unit -> unit
