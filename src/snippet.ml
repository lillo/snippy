let optional_arg none_case = function
    None -> none_case  
  | Some a -> a

let check_param invalid_value param msg = 
  if param = invalid_value then
    raise ( Failure msg )
    
let check_empty = check_param ""    
let bool_optional_arg = optional_arg false
let int64_optional_arg = optional_arg (Int64.of_int(-1))
    
class author ?id nickname name_last_name email  = 
  object(self)
    method get_name_last_name () =  ( name_last_name : string )
    method get_email () = ( email : string )
    method get_id () = int64_optional_arg id 
    method get_nickname () = ( nickname : string )
      
      initializer
        check_empty nickname "Author nickname is empty" 
  end

class ccode ?id (text: string) = 
  object(self)
    val mutable identifier = int64_optional_arg id 
    method get_id () = identifier
    method set_id (newid:int64) = 
      if Int64.compare newid (Int64.of_int 0) < 0 then 
	raise ( Failure  "code id negative" )
      else 
	identifier <- newid
    method get_text () = text
      
      initializer
        check_empty text "code text is empty"
  end


class cexception ?id constructor_name constructor_type text = 
  object(self)
    inherit ccode ~id:( int64_optional_arg id ) text as super

    method get_constructor_name () = (constructor_name:string)
    method get_constructor_type () = (constructor_type:string)
      
      initializer
        check_empty constructor_name "exception constructor name is empty" 
  end


class ctype ?id type_name ?polimorphic ?variant ?module_signature ?class_type text = 
  object(self)
    inherit ccode ~id:( int64_optional_arg id ) text as super

    method get_name () = ( type_name:string )
    method is_polimorphic () = bool_optional_arg polimorphic 
    method is_variant () = bool_optional_arg variant 
    method is_module_signature () = bool_optional_arg module_signature 
    method is_class_type () = bool_optional_arg class_type
      
      initializer 
        check_empty type_name "type name is empty" 
  end

class cclass ?id name ?is_virtual text = 
  object(self)
    inherit ccode ~id:( int64_optional_arg id ) text as super

    method get_name () = ( name:string )
    method is_virtual () = bool_optional_arg is_virtual
      
      initializer
        check_empty name "class name is empty" 
  end

class cmodule ?id name alias text = 
  object(self)
    inherit ccode ~id:( int64_optional_arg id ) text

    method get_name () = ( name:string )
    method get_alias () = ( alias:string )
      
      initializer  
          check_empty name "module name is empty"
  end

class cfunction ?id name f_type text = 
  object(self)
    inherit ccode ~id:( int64_optional_arg id ) text

    val mutable tests = ([] : (string * string) list)
    
    method get_name () = (name:string)
    method get_type () = (f_type:string)
    method get_tests () = tests
    method set_tests newtests = tests <- newtests
      
      initializer
        check_empty name "function name is empty";
        check_empty f_type "function type is empty"
  end

type code = 
  Code of ccode |
  Function of cfunction |
  Exception of cexception |
  Type of ctype |
  Class of cclass |
  Module of cmodule
    
let code2ccode = function
    Code(c) -> c
  | Function(f) -> (f :> ccode)
  | Exception(e) -> (e :> ccode)
  | Type(t) -> (t :> ccode)
  | Class(cl) -> (cl :> ccode)
  | Module(m) -> (m :> ccode)  
  


class snippet ?id ?parent ?deprecated ?snippet_tree name 
  brief version keywords licences use_example path signature authors 
  depend_list cl = 
  object(self)

		method get_id () = int64_optional_arg id 
    method get_name () = (name : string)
    method get_brief () = (brief : string)
    method get_version () = (version: string)
    method get_keywords () = (keywords : string list)
    method get_licenses () = (licences : string list)
    method get_use_example () = (use_example : string)
    method get_path () = (path : string)
    method get_dir () = ""
    method get_authors () = (authors: author list)
    method get_code () = (cl : code list)
    method get_parent () = int64_optional_arg parent
    method get_snippet_tree () = int64_optional_arg snippet_tree
    method is_deprecated () = bool_optional_arg deprecated
    method get_depend_list () = (depend_list : int64 list)  
		method get_signature () = (signature : string)
      
    method to_text () = 
      let f_aux txt code = 
        Printf.sprintf "%s\n%s" txt ((code2ccode code)#get_text())
      in
      List.fold_left f_aux "" cl 
      
      
    initializer 
      check_empty name "Snippet name is empty"
  end

let equals s1 s2 = 
    (Int64.compare (s1#get_id ()) (s2#get_id()) ) = 0