open Arg

module DB = Dbi_mysql 

let config_file = ".snippet_manager.conf"

let host = ref None
let port = ref None
let user = ref None
let password = ref None
let dbname = ref "SnippetsManager"

let get_default_connection () = 
  match !host, !port, !user, !password with
      (None, None, None, None ) -> new DB.connection !dbname
    | (Some(h), None, None, None ) -> new DB.connection ~host:h !dbname
    | (None, Some(p), None, None ) -> new DB.connection ~port:p !dbname
    | (None, None, Some(u), None ) -> new DB.connection ~user:u !dbname
    | (None, None, None, Some(p) ) -> new DB.connection ~password:p !dbname
    | (Some(h), Some(p), None, None) -> new DB.connection ~host:h ~port:p !dbname
    | (Some(h), None, Some(u), None) -> new DB.connection ~host:h ~user:u !dbname
    | (Some(h), None, None, Some(p) ) -> new DB.connection ~host:h ~password:p !dbname
    | (None, Some(p), Some(u), None ) -> new DB.connection ~port:p ~user:u !dbname
    | (None, Some(p), None, Some(pa)) -> new DB.connection ~port:p ~password:pa !dbname
    | (None, None, Some(u), Some(p) ) -> new DB.connection ~user:u ~password:p !dbname
    | (Some(h), Some(p), Some(u), None) -> new DB.connection ~host:u ~port:p ~user:u !dbname
    | (Some(h), Some(p), None, Some(pa)) -> new DB.connection ~host:h ~port:p ~password:pa !dbname
    | (Some(h), None, Some(u), Some(p)) -> new DB.connection ~host:h ~user:u ~password:p !dbname
    | (None, Some(p), Some(u), Some(pa))-> new DB.connection ~port:p ~user:u ~password:pa !dbname
    | (Some(h),Some(p),Some(u),Some(pa))-> new DB.connection ~host:h ~port:p ~user:u ~password:pa !dbname


let set_hostname (host_name : string)= 
  host := Some(host_name)

let set_port (db_port : string)= 
  port := Some(db_port)

let set_user (db_user : string) =
  user := Some(db_user)

let set_password (db_pass : string) =
  password := Some(db_pass)

let set_dbname (name : string) =
  if name <> "" then
    dbname := name

let write_line out_channel key value = 
  Printf.fprintf out_channel "%s:%s\n" key value

let get_conf_file_channel fopen =
  let conf_file = Printf.sprintf "%s/%s" (Sys.getenv "HOME") config_file in
    fopen conf_file 

let write_conf_file () = 
  try
   let channel = get_conf_file_channel open_out in
      begin
	(match !host with
	    Some(h) -> write_line channel "host" h
	  | _ -> ());
	 (match !port with
	      Some(p) -> write_line channel "port" p
	    | _ -> ());
	 (match !user with
	      Some(u) -> write_line channel "user" u
	    | _ -> ());
	 write_line channel "dbname" !dbname
      end
  with Not_found -> assert false

let read_config_file () = 
  try
    let channel = get_conf_file_channel open_in in
      while true do
	let line = input_line channel 
	and regexp = Str.regexp ":" in
	let l = Str.split regexp line in
	let key = List.hd l 
	and value = List.hd (List.tl l) in
	  match key with
	      "host" -> host := Some(value)
	    | "port" -> port := Some(value)
	    | "user" -> user := Some(value)
	    | "dbname" -> dbname := value
	    | _ -> ()
      done
  with Not_found -> write_conf_file ()
    | Sys_error(d) -> write_conf_file ()
    | End_of_file -> ()
    | Failure(msg) -> ()

     


let db_config_arg_list = [
  ("-dbhost", String(set_hostname), "Database hostname") ;
  ("-dbport", String(set_port), "Database port number" )    ;
  ("-dbuser", String(set_user), "Database username" )    ;
  ("-dbpass", String(set_password), "Database password") ;
  ("-dbname", String(set_dbname), "Database name")
]



