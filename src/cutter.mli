val tree_root : Tree.node ref
val insert_into_tree : Snippet.snippet list -> unit
val module_snippet :
  < get_path_of_depend_clos : Snippet.snippet -> string list; .. > -> string
val create_module_snippet :
  < get_depend_closure : Snippet.snippet ->
                         ('a -> bool) -> Snippet.snippet list;
    get_path_of_depend_clos : Snippet.snippet -> string list; .. > ->
  string
val create_preamble : unit -> string
val create_module_preamble : string -> string
val ocamldoc_invisible_area : string -> string -> string
val create_module_data : string -> string
val write_on_file : out_channel -> string -> string -> unit
val error_msg : string -> string -> unit
val analyse_files : Repository.db_repository -> string list -> unit
val extract_snippets : Repository.db_repository -> string -> unit
val usage_msg : string
val main : unit -> unit
