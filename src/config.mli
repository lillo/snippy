module DB :
  sig
    class connection :
      ?host:string ->
      ?port:string ->
      ?user:string ->
      ?password:string ->
      string ->
      object
        method close : unit -> unit
        method closed : bool
        method commit : unit -> unit
        method database : string
        method database_type : string
        method databases : string list
        method debug : bool
        method ex : string -> Dbi.sql_t list -> Dbi.statement
        method host : string option
        method id : int
        method password : string option
        method ping : unit -> bool
        method port : string option
        method prepare : string -> Dbi.statement
        method prepare_cached : string -> Dbi.statement
        method register_postrollback :
          (unit -> unit) -> Dbi.postrollback_handle
        method register_precommit : (unit -> unit) -> Dbi.precommit_handle
        method rollback : unit -> unit
        method set_debug : bool -> unit
        method tables : string list
        method unregister_postrollback : Dbi.postrollback_handle -> unit
        method unregister_precommit : Dbi.precommit_handle -> unit
        method user : string option
      end
    val connect :
      ?host:string ->
      ?port:string ->
      ?user:string -> ?password:string -> string -> connection
    val close : connection -> unit
    val closed : connection -> bool
    val commit : connection -> unit
    val ping : connection -> bool
    val rollback : connection -> unit
  end
val config_file : string
val host : string option ref
val port : string option ref
val user : string option ref
val password : string option ref
val dbname : string ref
val get_default_connection : unit -> DB.connection
val set_hostname : string -> unit
val set_port : string -> unit
val set_user : string -> unit
val set_password : string -> unit
val set_dbname : string -> unit
val write_line : out_channel -> string -> string -> unit
val get_conf_file_channel : (string -> 'a) -> 'a
val write_conf_file : unit -> unit
val read_config_file : unit -> unit
val db_config_arg_list : (string * Arg.spec * string) list
