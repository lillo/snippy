{ 
  open Repository
  open Snippet
  
  let delete_ends str = 
    let s_index = String.index str '.'
    and e_index = String.rindex str '.' in
      String.sub str (s_index + 1) (e_index - s_index - 1)
  
  let get_dir_snippet_name str = 
    let s = delete_ends str in
      let dot = String.rindex s '.' in
        let dir = String.sub s 0 dot
        and name = String.sub s (dot + 1) (String.length s - dot - 1) in
          dir, name
					
	let get_snippet repo str = 
		let tmp_dir, tmp_name = get_dir_snippet_name str in
       let name = String.lowercase tmp_name
       and dir = Str.global_replace (Str.regexp "\.") "/" (String.lowercase tmp_dir) in
       repo#get_snippet_by_path name dir (fun x -> true)
  
  let snippet_list = ref ([])
  
}
  let letter = ['a'-'z''A'-'Z']
  let digit = ['0'-'9']
  let ide = (letter) (letter | digit | '_' )*
  let matcher = "Snippet." (ide ".")* ide 
    
  rule analyse repo = parse
    matcher as s 
      { 
  				try
					let (snippet:snippet) = get_snippet repo s in
            snippet_list := !snippet_list @ [snippet];
          analyse repo lexbuf
					with a -> Printf.fprintf stderr "stavo cercando %s\n" s; assert false
      }
    | _  { analyse repo lexbuf }
    |eof { !snippet_list }
      
{

}
