val create_open : string -> string -> string
val create_module_snippet :
  < get_path_of_depend_clos : Snippet.snippet -> string list; .. > ->
  Snippet.snippet -> string
type node = Leaf of Snippet.snippet | Node of (string * node list)
val create_node : string -> node
val insert : node list -> Snippet.snippet -> node list
val next : string -> string
val current : string -> string
val add_node : node -> string -> Snippet.snippet -> node
val walk :
  < get_path_of_depend_clos : Snippet.snippet -> string list; .. > ->
  node -> string
val walk2 : 'a -> node -> string
val root : node
val depend_closure :
  < get_depend_closure : Snippet.snippet ->
                         ('a -> bool) -> Snippet.snippet list;
    .. > ->
  Snippet.snippet list -> Snippet.snippet list
