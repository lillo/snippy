open Mysql_schema
open Config

let query_exec db_conn query =
	let q = db_conn#prepare query in
	q#execute []

let usage_msg = Printf.sprintf "Usage:\n\n%s [db_options]\n\nWhere db_options are:" Sys.argv.(0)
 
let main () =
	Arg.parse Config.db_config_arg_list (fun x -> ()) usage_msg;
	let conn = Config.get_default_connection ()
	and create_db = Mysql_schema.create_db_query !Config.dbname
	and delete_db = Mysql_schema.delete_db_query !Config.dbname
	and use_db = Mysql_schema.use_db_query !Config.dbname in
	try
		List.iter (query_exec conn)
		(delete_db :: create_db :: use_db :: Mysql_schema.create_table_query);
		Config.write_conf_file ()
	with Mysql.Error(s) -> Printf.fprintf stderr "%s\n" s
 
let _ = main ()
 
