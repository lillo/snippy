open Snippet
open Repository

let create_open path1 path2 = 
	let regexp = Str.regexp_string path1 in
	let path3 = Str.global_replace regexp "" path2 in
	let regexp2 = Str.regexp "/" in
	let l = Str.split regexp2 path3 in
	let f_aux t_res str = 
		Printf.sprintf "%s.%s" t_res (String.capitalize str) in
	Printf.sprintf "open %s\n" (List.fold_left f_aux (String.capitalize (List.hd l)) (List.tl l))

let create_module_snippet repo (s: snippet) = 
	let dir_of_depend_clos = repo#get_path_of_depend_clos s in
	let open_dir = List.map (create_open (s#get_path())) dir_of_depend_clos in
		Printf.sprintf "module %s = \nstruct\n%s\n%s\nend" (String.capitalize (s#get_name())) 
				(List.fold_left (^) "" open_dir) (s#to_text())
				

type node = Leaf of snippet | Node of ( string * node list )

let create_node label = Node(label,[])

let insert l s = 
let f_aux = function Leaf(a) -> Snippet.equals s a | _ -> false in
    if List.exists f_aux l then
      l 
    else
      l @ [Leaf(s)]

let next label = 
  try
    let p = String.index  label '/' in
      String.sub label (p+1) (String.length label - p - 1)
  with  Not_found -> ""

let current label =
  try
    let p = String.index label '/' in
      String.sub label 0 p 
  with Not_found -> label

let rec add_node tree label s = 
  match tree with
      Node(el,node_list) ->
	(if el = (current label) then
	  Node(el, insert node_list  s )
	else
	  let rec f_aux = function
	      []  -> raise Not_found
	    | (Node(el,_) as n)::t -> 
		 if el = (current label) then
		   (add_node n (next label) s)::t
		 else
		   n::f_aux t
	    | (Leaf(_) as l)::t -> l::f_aux t
	  in
	    try
	      Node(el, f_aux node_list)
	    with Not_found ->
	      let new_node = Node(current label, []) in 
	      let new_node = add_node new_node (next label) s in
		Node(el, node_list @ [new_node])
	)
    | _ -> assert false

let rec walk repo tree =
  match tree with
      Node(el, node_list) -> 
	let fl str node = 
	  Printf.sprintf "%s\n%s\n" str (walk repo node)
	in
	  if el = "" then
	    Printf.sprintf "%s\n" (List.fold_left fl "" node_list)
	  else
			if el = "/" then
	    Printf.sprintf "module rec Snippet : TSnippet = struct \n%s\nend\n" (List.fold_left fl "" node_list)
			else
	    Printf.sprintf "module %s = \nstruct\n%s\nend\n" 
	      (String.capitalize el) (List.fold_left fl "" node_list)
               | Leaf(s) -> create_module_snippet repo s

let rec walk2 repo tree =
  match tree with
      Node(el, node_list) -> 
	let fl str node = 
	  Printf.sprintf "%s\n%s\n" str (walk2 repo node)
	in
	  if el = "" then
	    Printf.sprintf "%s\n" (List.fold_left fl "" node_list)
		else 
			if el = "/" then
				Printf.sprintf "module type TSnippet = sig \n%s\n end\n" (List.fold_left fl "" node_list)
	  else
	    Printf.sprintf "module %s : \nsig\n%s\nend\n" 
	      (String.capitalize el) (List.fold_left fl "" node_list)
               | Leaf(s) -> s#get_signature ()


let root = create_node "/"

let rec depend_closure repo = function
    ([] : snippet list) -> ([] : snippet list)
  | h::t ->
  let (clos_dep:snippet list) = (repo#get_depend_closure h (fun x -> true)) in
      (depend_closure repo clos_dep) @ [h] @ depend_closure repo t
  